package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

//import java.util.random.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        //Game loop
        while(true) {
            // TODO: Fjerna \n?
            System.out.printf("Let's play round %s", roundCounter + "\n");
            // Human and Computer choice
            String human_choice = user_choice();
            String computer_choice = random_choice();
            String choice_string = "Human chose " + human_choice + ", computer chose " + computer_choice + ".";
        
            // Check who won
            if (is_winner(human_choice, computer_choice)) {
                System.out.printf("%s Human wins! \n", choice_string);
                humanScore++;
            }
            else if (is_winner(computer_choice, human_choice)) {
                System.out.printf("%s Computer wins! \n", choice_string);
                computerScore++;
            }
            else
                System.out.print(choice_string + " It's a tie! \n");
            System.out.printf("Score: human %s, computer %s \n", humanScore, computerScore);
        
            // Ask if human wants to play again
            String continue_answer = continue_playing();
            if (continue_answer.equals("n")) {
                System.out.print("Bye bye :)");
                break;
            }
        
        }


    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String random_choice() {
        int random = new Random().nextInt(rpsChoices.size());

        return rpsChoices.get(random);

    }

    public boolean is_winner(String choice1, String choice2) {
    
        if (choice1.equals("paper"))
            return choice2.equals("rock"); //choice2.equals("rock");
        else if(choice1.equals("scissors"))
            return choice2.equals("paper");
        else
            return (choice2.equals("scissors"));
    
    }

    public boolean validate_input(String input, List<String> valid_input) {
        input = input.toLowerCase();
        return valid_input.contains(input);
    }

    public String user_choice() {
    
        while(true) {
            String human_choice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validate_input(human_choice, rpsChoices))
                return human_choice;
            else
                System.out.print("I don't understand " + human_choice + ". Try again. ");
        }
    
    }

    public String continue_playing() {
    
        while(true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            //if (continue_answer == "y" || continue_answer == "n")
            if (continue_answer.equals("y") || continue_answer.equals("n")) {
                roundCounter++;
                return continue_answer;
            }    
            else
                System.out.print("I don't understand " + continue_answer + ". Try again");
        }
    }


    

}